const nodemailer = require('nodemailer');
var request = require('request');

exports.Message = function (form) {

  this.seeVar = function () {
    console.log(form)
  }

  this.savetorobot = function () {

    for (key in form) {
      if (form[key] == "") {
        delete form[key];
      }

    }
    // insert into SQL
    let robotapi = '';
    let body = {
      strProjectId: '{}',
      nLanguageId: 950,
      strFuncPdno: '',
      strCompanyId: '{}',
      oUserDefParam: {
        //api 對應
        classify: form.classify,
        companyname: form.companyname,
        storename: form.storename,
        officeaddress: form.officeaddress,
        buesinessowner: form.buesinessowner,
        business: form.business,
        ownership: form.ownership,
        dtino: form.dtino,
        dtidate: form.dtidate,
        secno: form.secno,
        secdate: form.secdate,
        tinno: form.tinno,
        paidupcapital: form.paidupcapital,
        landline: form.landline,
        email: form.email,
        mobile: form.mobile,
        fax: form.fax,
        designation: form.designation,
        brgyclearance: form.brgyclearance,
        stikerfee: form.stikerfee,
        miscfee: form.miscfee,
        total: form.total,
        verificationcode: form.verificationcode
      },
      strAccount: '',
      strPassword: ''
    }
    return new Promise((resolve, reject) => {

      request.post({
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        url: robotapi,
        form: JSON.stringify(body)
      }, function (error, response, body) {
        if (error) {
          console.log(error);
          reject(error);
        }
        //回傳值判斷
        let resval = JSON.parse(body).bSuccess;
        if (resval) {
          console.log('Insert to DB success!');
          resolve();

        } else {
          console.log('Insert to DB fail!');
          console.log(JSON.parse(body).error)
          reject(JSON.parse(body).error);
        }

      });

    });

  }

  this.sendMail = function () {

    console.log(form)

    for (key in form) {
      if (form[key] == '') {
        form[key] = "N/A";
      }

    }



    let transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: '',
        pass: ''
      }
    });

    let options = {
      form: 'sys@wattertek.com', //寄件者
      to: form.email, //收件者
      // cc: '', //副本
      //bcc: '', //密件副本
      subject: 'ONLINE BARANGAY BUSINESS CLEARANCE APPLICATION : ' + form.companyname, //主旨
      //嵌入 html 的內文
      html: `
      <a style="color:black;margin:10px;">Thank you for using the Online Barangay Business Clearance Mobile App.</a>
      <p style="color:black;margin:10px;">This is to inform you that you have successfully submitted your application to Barangay San Lorenzo with the following details:</p>
      <br>
      <br>
      <a style="color:black;margin:10px;"> Please take note your Verification Code:</a>
      <p style="color:blue;margin:10px;font-size:25px;border-style: solid;border-color: black;width: 100px;text-align:  center;"> ${form.verificationcode}</p>
      <br>
      <img src="cid:header" style="width: 1006px">
      <div style="background-image: url(cid:layout);word-wrap:break-word;width: 1006px;background-size: cover;">
        <div>
          <p style="margin:10px;margin-bottom:20px;font-size:25px;font-weight:bold;">Business information : </p>
          <b style="color:black;margin:10px;"> Classifiy:</b>
          <a style="color:black;margin:10px;"> ${form.classify}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Company Name:</b>
          <a style="color:black;margin:10px;"> ${form.companyname}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Store Name:</b>
          <a style="color:black;margin:10px;"> ${form.storename}</a>
          <p></p>
          <b style="color:black;margin:10px;"> MAKATI Office Address / Business Address :</b>
          <a style="color:black;margin:10px;"> ${form.officeaddress}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Business Owner:</b>
          <a style="color:black;margin:10px;"> ${form.buesinessowner}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Lin Of Business:</b>
          <a style="color:black;margin:10px;"> ${form.business}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Type Of Ownership:</b>
          <a style="color:black;margin:10px;"> ${form.ownership}</a>
          <p></p>
          <b style="color:black;margin:10px;"> DTI NO.:</b>
          <a style="color:black;margin:10px;"> ${form.dtino}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Date Of Issue:</b>
          <a style="color:black;margin:10px;"> ${form.dtidate}</a>
          <p></p>
          <b style="color:black;margin:10px;"> SEC NO.:</b>
          <a style="color:black;margin:10px;"> ${form.secno}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Date Of Issue:</b>
          <a style="color:black;margin:10px;"> ${form.secdate}</a>
          <p></p>
          <b style="color:black;margin:10px;"> TIN NO.:</b>
          <a style="color:black;margin:10px;"> ${form.tinno}</a>
          <p></p>
        </div>
        <div>
          <p style="margin:10px;margin-bottom:20px;font-size:25px;font-weight:bold;">Content Detail : </p>
          <b style="color:black;margin:10px;"> Landline NO.:</b>
          <a style="color:black;margin:10px;"> ${form.landline}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Email Address:</b>
          <a style="color:black;margin:10px;"> ${form.email}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Mobile No.:</b>
          <a style="color:black;margin:10px;"> ${form.email}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Fax No.:</b>
          <a style="color:black;margin:10px;"> ${form.fax}</a>
          <p></p>
          <b style="color:black;margin:10px;"> Person To Contact / Designation:</b>
          <a style="color:black;margin:10px;"> ${form.designation}</a>
          <p></p>
        </div>
        <div>
          <p style="margin:10px;margin-bottom:20px;font-size:25px;font-weight:bold;">Barangay Business Clearance Computation Fee: </p>
          <table border="1" style="border-collapse:collapse;margin: 10px">
            <tr>
              <td colspan="2" style="text-align: center">Computation</td>
            </tr>
            <tr>
              <td style="width: 150px">Pid-Up Capital</td>
              <td style="width: 75px">${form.paidupcapital}</td>
            </tr>
            <td>Barangay Clearance</td>
            <td>${form.brgyclearance}</td>
            <tr>
              <td>Sticker Fee</td>
              <td>${form.stikerfee}</td>
            </tr>
            <td>Misc Fee</td>
            <td>${form.miscfee}</td>
            <tr>
              <td style="font-size: 20px;font-weight:bold;">TOTAL</td>
              <td style="font-size: 20px;font-weight:bold;color:blue">${form.total}</td>
            </tr>
          </table>
      
        </div>
        <h4>For any inquiries, you may call 541-1163 to 67 loc 108 or 109 or message and call us directly using our Loop-Conect Mobile
          App.
        </h4>
      </div>
      <img src="cid:footer" style="width: 1006px;hight:">
            `,
      attachments: [{
        filename: 'layout.png',
        path: './public/images/layout.png',
        cid: 'layout'
      }, {
        filename: 'header.png',
        path: './public/images/header.png',
        cid: 'header'
      }, {
        filename: 'footer.png',
        path: './public/images/footer.png',
        cid: 'footer'
      }]

    };



    return new Promise((resolve, reject) => {


      transporter.sendMail(options, function (err, info) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          console.log('Send mail finished');
          resolve();
        }
      });

    })
  }


}