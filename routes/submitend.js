var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {

  let code = req.query.code
  let amount = req.query.amount
  //console.log(code)

  
  res.render('submitend',{
    title:"Clearance",
    code:code,
    amount:amount
  });
});

module.exports = router;