var express = require('express');
const nodemailer = require('nodemailer');
var router = express.Router();
var request = require('request')
const msghandler = require('./msghandler');


/* GET home page. */
router.get('/', function (req, res, next) {
  let list = ['Advertising', 'Agricultural', 'Airlines', 'Banks', 'Call Center', 'Canteen', 'Construction', 'Convenience Store', 'Cooperative', 'Customs Brokerage', 'Distributor', 'Educational Institution', 'Exporter', 'Financing Institution', 'Food Chain/Kiosk', 'Foreign Exchange Dealer', 'Foundation', 'Freight Forwarding', 'Holding Company', 'Hotel/Apartelles', 'Importer', 'Insurance Broker', 'Incestment', 'Jolly Jeep/Metro Store', 'Mailing Address only', 'Manufacturer', 'Manpower/Recuiting', 'Merchandise-Retailer', 'Merchandise-Wholesaler', 'Mining', 'Music Lounge Bar', 'Non-stock/Non-Profit', 'Pawnshop', 'Pre-need Company', 'Real Estate Dealer', 'Real Esate Developer', 'Real Estate Lessor', 'Respresentative/Regional Office', 'Restaurant', 'Security Agency', 'Shopping Center', 'Stock Brokerage', 'Trading', 'Wholesaler', 'Other']
  let businesslist = JSON.stringify(list).replace(/"/g, '\'');





  res.render('index', {
    title: 'SAN LORENZO',
    businesslist: businesslist
  });
});


router.post('/', function (req, res, next) {


  //Generate verification code

  let verifycode = getverifycode();

  function getverifycode() {
    var code = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (var i = 0; i < 5; i++) {
      code += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return code
  }

  let form = {
    classify: req.body.classify,
    companyname: req.body.companyname,
    storename: req.body.storename,
    officeaddress: req.body.officeaddress,
    buesinessowner: req.body.buesinessowner,
    business: req.body.business,
    ownership: req.body.ownership,
    dtino: req.body.dtino,
    dtidate: req.body.dtidate,
    secno: req.body.secno,
    secdate: req.body.secdate,
    tinno: req.body.tinno,
    paidupcapital: req.body.paidupcapital,
    landline: req.body.landline,
    email: req.body.email,
    mobile: req.body.mobile,
    fax: req.body.fax,
    designation: req.body.designation,
    brgyclearance: req.body.brgyclearance,
    stikerfee: req.body.stikerfee,
    miscfee: req.body.miscfee,
    total: req.body.total,
    verificationcode: verifycode
  }


  //console.log(form)

  let msg = new msghandler.Message(form)
  msg.seeVar();
  msg.savetorobot()
    .then(() => {
      res.redirect('submitend?code=' + verifycode + "&amount=" + form.total)
      msg.sendMail();
    })
    .catch(error => res.render('error', {
      message: "Sorry!!There is an error in data transfer. Please resubmit your application and try again"
    }));







});
module.exports = router;