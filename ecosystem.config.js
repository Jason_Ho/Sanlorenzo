module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'BSL',
      script    : 'bin/www',
      watch     : true,
      instances  : 1,
      exec_mode : "fork"
    }
  ]
};
